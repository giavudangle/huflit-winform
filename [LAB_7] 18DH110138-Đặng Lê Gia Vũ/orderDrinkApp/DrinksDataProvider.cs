﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace orderDrinkApp
{
    class DrinksDataProvider
    {
        private static readonly string connectionString = "Data Source=ADMIN;Initial Catalog=QLQUAN;Integrated Security=True";
        private static SqlConnection connection = new SqlConnection(connectionString);
        public DrinksDataProvider() { }

        #region Fetch List Drinks Method
        public void FetchListDrinks()
        {        
            string queryString =
               "SELECT * FROM DRINKS";
     
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
               
                SqlCommand command = new SqlCommand(queryString, connection);        
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();                
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.ReadLine();
            }
        }
        #endregion

        public SqlDataReader FetchDrink()
        {
            SqlCommand command = new SqlCommand("SELECT * FROM DRINKS", connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();         
            return reader;

        }

        #region Add Drink Method
        public void AddDrink(string drinkID, string drinkName, int price, int active)
        {
            // Provide the query string with a parameter placeholder.
            string queryString =
               "INSERT INTO DRINKS(MSHH,TenHang,Gia,TinhTrang)" +
                "VALUES(@drinkID,@drinkName,@price, @active)";

            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
               
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.AddWithValue("@drinkID", drinkID);
                command.Parameters.AddWithValue("@drinkName", drinkName);
                command.Parameters.AddWithValue("@price", price);
                command.Parameters.AddWithValue("@active", active);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            
                Console.WriteLine("Add Drink Sucessfully");
            }
        }
        #endregion
        #region Delete Drink Method
        public void DeleteDrink(string drinkID)
        {
            // Provide the query string with a parameter placeholder.
            string queryString = "DELETE FROM DRINKS WHERE MSHH=@drinkID";



            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@drinkID", drinkID);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.WriteLine("\t{0}\t{1}\t{2}\t{3}",
                            reader[0], reader[1], reader[2], reader[3]);
                    }

                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                //Console.ReadLine();
                Console.WriteLine("Delete Drink Sucessfully");
            }
        }
        #endregion
        #region Edit Drink Method
        public void EditDrink(string drinkID, string drinkName, int active, int price)
        {
            // Provide the query string with a parameter placeholder.
            string queryString = "UPDATE DRINKS " +
                                    "SET Gia = @price, TinhTrang = @active, TenHang =@drinkName" +
                                    " WHERE MSHH=@drinkID";




            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@drinkID", drinkID);
                command.Parameters.AddWithValue("@drinkName", drinkName);
                command.Parameters.AddWithValue("@active", active);
                command.Parameters.AddWithValue("@price", price);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.WriteLine("\t{0}\t{1}\t{2}\t{3}",
                            reader[0], reader[1], reader[2], reader[3]);
                    }

                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                //Console.ReadLine();
                Console.WriteLine("Edit Drink Sucessfully");
            }
        }
        #endregion

    }
}
