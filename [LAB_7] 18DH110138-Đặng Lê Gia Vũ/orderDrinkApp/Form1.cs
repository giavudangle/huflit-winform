﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace orderDrinkApp
{
    public partial class Form1 : Form
    {
        private static DrinksDataProvider drinksDataProvider = new DrinksDataProvider();
        private static OrderDataProvider orderDataProvider = new OrderDataProvider();
        private static BillsDataProvider billsDataProvider = new BillsDataProvider();

        private static readonly string
        connectionString = "Data Source=ADMIN;Initial Catalog=QLQUAN;Integrated Security=True";
        private static SqlConnection connection = new SqlConnection(connectionString);      
        public string ID;

       


        public Form1()
        {
            InitializeComponent();
            //getData_ListDrink();
            getData_ListDrinkTmp();
            getData_OrderDrink();
            loadDrink_ComboBox();

        }

        private void getData_ListDrink()
        {
            SqlCommand command = new SqlCommand("SELECT * FROM DRINKS", connection);
            DataTable dt = new DataTable();
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            dt.Load(reader);
            connection.Close();
            listDataDrink.DataSource = dt;
            this.listDataDrink.Columns[0].HeaderText = "Product ID";
            this.listDataDrink.Columns[1].HeaderText = "Product Name";
            this.listDataDrink.Columns[2].HeaderText = "Product Price";
            this.listDataDrink.Columns[3].HeaderText = "Product Status";
        }

        private void getData_ListDrinkTmp()
        {
            DataTable dt = new DataTable();
            dt.Load(drinksDataProvider.FetchDrink());
            listDataDrink.DataSource = dt;
            this.listDataDrink.Columns[0].HeaderText = "Product ID";
            this.listDataDrink.Columns[1].HeaderText = "Product Name";
            this.listDataDrink.Columns[2].HeaderText = "Product Price";
            this.listDataDrink.Columns[3].HeaderText = "Product Status";

        }



        private bool isValid()
        {
            if (textBox_Name.Text == string.Empty)
            {
                MessageBox.Show("Require product name!", "Failed",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
            if (textBox_price.Text == string.Empty)
            {
                MessageBox.Show("Require Price!", "Failed",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
            if (textBox_productID.Text == string.Empty)
            {
                MessageBox.Show("Require product ID!", "Failed",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
            if (textBox_status.Text == string.Empty)
            {
                MessageBox.Show("Require Status!", "Failed",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
            return true;
        }


        private void resetContent_ListDrink()
        {
            textBox_Name.Text = "";
            textBox_price.Text = "";
            textBox_productID.Text = "";
            textBox_status.Text = "";
        }

        private void listDataDrink_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            ID = listDataDrink.SelectedRows[0].Cells[0].Value.ToString();
            textBox_productID.Text = listDataDrink.SelectedRows[0].Cells[0].Value.ToString();
            textBox_Name.Text = listDataDrink.SelectedRows[0].Cells[1].Value.ToString();
            textBox_price.Text = listDataDrink.SelectedRows[0].Cells[2].Value.ToString();
            textBox_status.Text = listDataDrink.SelectedRows[0].Cells[3].Value.ToString();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {


            if (isValid())
            {

                string drinkID = textBox_productID.Text;
                string drinkName = textBox_Name.Text;
                int price = int.Parse(textBox_price.Text);
                int status = int.Parse(textBox_status.Text);
                drinksDataProvider.AddDrink(drinkID, drinkName, price, status);
                getData_ListDrink();
                MessageBox.Show("Add Product Sucessfully");
                resetContent_ListDrink();
            }
        }



        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (ID != String.Empty)
            {
                string queryString = "DELETE FROM DRINKS WHERE MSHH=@drinkID";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@drinkID", textBox_productID.Text);
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();

                getData_ListDrink();
                MessageBox.Show("DELETE Product Successfully!", "Deleted",
                  MessageBoxButtons.OK,
                      MessageBoxIcon.Information);
                resetContent_ListDrink();
            }
            else
            {
                MessageBox.Show("DELETE ERROR !", "ERROR",
                   MessageBoxButtons.OK,
                       MessageBoxIcon.Error);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {


            if (ID != String.Empty)
            {
                string queryString = "UPDATE DRINKS SET TinhTrang=@status,Gia=@price,TenHang=@drinkName " +
                    " WHERE MSHH=@drinkID";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@price", textBox_price.Text);
                command.Parameters.AddWithValue("@drinkName", textBox_Name.Text);
                command.Parameters.AddWithValue("@status", textBox_status.Text);
                command.Parameters.AddWithValue("@drinkID", textBox_productID.Text);
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();

                MessageBox.Show("Update Product Successfully!", "Updated",
                    MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                getData_ListDrink();
                resetContent_ListDrink();
            }
            else
            {
                MessageBox.Show("Update ERROR !", "ERROR",
                   MessageBoxButtons.OK,
                       MessageBoxIcon.Error);
            }
        }
        private void getData_OrderDrink()
        {
            SqlCommand command = new SqlCommand("SELECT * FROM ORDER_DRINKS", connection);
            DataTable dt = new DataTable();
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            dt.Load(reader);
            connection.Close();
            listOrder.DataSource = dt;
            this.listOrder.Columns[0].HeaderText = "Order ID";
            this.listOrder.Columns[1].HeaderText = "Product ID";
            this.listOrder.Columns[2].HeaderText = "Quantity";
            this.listOrder.Columns[3].HeaderText = "Sale(%)";
        }

      
        private void loadDrink_ComboBox()
        {
            SqlCommand command = new SqlCommand("SELECT TenHang FROM DRINKS", connection);          
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                cbx_DrinkName.Items.Add(reader["TenHang"].ToString());
            }
            
            connection.Close();
            
            
        }

        private void btn_AddOrder_Click(object sender, EventArgs e)
        {
            string orderID = txt_orderID.Text;
            string drinkName = cbx_DrinkName.Text;
            
            string productID = selectID_FromName(drinkName);
            //MessageBox.Show(productID);
            int quantity = (int) numberic_Quantity.Value;
            int sale =(int) numeric_Sale.Value;
            orderDataProvider.AddOrder(orderID, productID, quantity, sale);
            MessageBox.Show("Create an Order Successfully");
            getData_OrderDrink();

        }

        private string selectID_FromName(string name)
        {
            string res = "";
            SqlCommand command = new SqlCommand("SELECT MSHH FROM DRINKS WHERE TenHang=@drinkName", connection);
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@drinkName", name);
            connection.Open();
                                
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                res=reader["MSHH"].ToString();
            }

            connection.Close();
            return res;               
        }

        private int selectPrice_FromID(string id)
        {
            int res = 0;
            SqlCommand command = new SqlCommand("SELECT Gia FROM DRINKS WHERE MSHH=@drinkID", connection);
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@drinkID", id);
            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                res = reader.GetInt32(0);
            }
            connection.Close();

            return res;
        }

        private void btn_Calculate_Click(object sender, EventArgs e)
        {
            double res = 0;
            // res = (productPrice * quantity) * (salePercent/100)

            string ID = selectID_FromName(cbx_DrinkName.Text);
            int getMoney = selectPrice_FromID(ID);
           
            int quantity = (int) numberic_Quantity.Value;
            int sale = (int)numeric_Sale.Value;    
            int temp= getMoney * quantity;
            double hihi = sale*1.0 / 100;

            res = temp - (temp * hihi);
           MessageBox.Show(res.ToString() + " VND", "Giá trị đơn hàng là",MessageBoxButtons.OK,MessageBoxIcon.Information);
                     
        }

        private string selectName_FromID(string ID)
        {
            string res = "";
            SqlCommand command = new SqlCommand("SELECT TenHang FROM DRINKS WHERE MSHH=@drinkID", connection);
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@drinkID", ID);
            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                res = reader["TenHang"].ToString();
            }

            connection.Close();
            return res;
        }


        private void listOrder_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {


            //ID = listDataDrink.SelectedRows[0].Cells[0].Value.ToString();
            //textBox_productID.Text = listDataDrink.SelectedRows[0].Cells[0].Value.ToString();
            //textBox_Name.Text = listDataDrink.SelectedRows[0].Cells[1].Value.ToString();
            //textBox_price.Text = listDataDrink.SelectedRows[0].Cells[2].Value.ToString();
            //textBox_status.Text = listDataDrink.SelectedRows[0].Cells[3].Value.ToString();

            // Get ID of drink from Name Combo Box
            string temp = selectID_FromName(cbx_DrinkName.Text);

            txt_orderID.Text= listOrder.SelectedRows[0].Cells[0].Value.ToString();
            //cbx_DrinkName.Text = listOrder.SelectedRows[0].Cells[1].Value.ToString(); // Get name drink
            cbx_DrinkName.Text = selectName_FromID(listOrder.SelectedRows[0].Cells[1].Value.ToString()); // Get name drink
            numberic_Quantity.Value = (int)listOrder.SelectedRows[0].Cells[2].Value;
            numeric_Sale.Value = (int)listOrder.SelectedRows[0].Cells[3].Value;
           
           
        }

      

        private void OnCloseOrder(object sender,FormClosedEventArgs e)
        {
            MessageBox.Show("CLose form");
            
        }
    }
}
