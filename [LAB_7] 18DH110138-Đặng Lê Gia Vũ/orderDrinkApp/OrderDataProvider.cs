﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace orderDrinkApp
{
    class OrderDataProvider
    {
        private static readonly string connectionString = "Data Source=ADMIN;Initial Catalog=QLQUAN;Integrated Security=True";

        #region FETCH LIST ORDER
        public void FetchListOrder()
        {

            // Provide the query string with a parameter placeholder.
            string queryString =
               "SELECT * FROM ORDER_DRINKS";
           
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
               
                SqlCommand command = new SqlCommand(queryString, connection);         
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    Console.WriteLine("OrderID\t\t\tDrinkID\t\t\tQuantity\t\t\tSale %");
                    while (reader.Read())
                    {
                        Console.WriteLine("{0}\t\t\t{1}\t\t\t{2}\t\t\t{3}",
                            reader[0], reader[1], reader[2], reader[3]);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.ReadLine();
            }
        }
        #endregion

        #region Create Bill
        public void AddOrder(string orderID, string drinkID, int quantity, int sale)
        {
            // Provide the query string with a parameter placeholder.
            string queryString =
               "INSERT INTO ORDER_DRINKS(MSDH, MSHH, SoLuong, TiLeGiam)" +
                " VALUES(@orderID,@drinkID,@quantity,@sale);";

            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.AddWithValue("@orderID", orderID);
                command.Parameters.AddWithValue("@drinkID", drinkID);
                command.Parameters.AddWithValue("@quantity", quantity);
                command.Parameters.AddWithValue("@sale", sale);

                // Open the connection in a try/catch block.
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                //Console.ReadLine();
                Console.WriteLine("Add Order Sucessfully");
            }
            #endregion
        }

        public string SelectID_FromName(string drinkName)
        {
            string queryString =
              "SELECT MSHH FROM DRINKS WHERE TenHang=N@drinkName";
            string res = "";
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue(@drinkName, drinkName);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    res = reader["MSHH"].ToString();
                    MessageBox.Show(res);
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                
            }



            return res;
        }
    }
}
