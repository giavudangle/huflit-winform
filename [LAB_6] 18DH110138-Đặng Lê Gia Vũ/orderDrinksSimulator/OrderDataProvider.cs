﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace orderDrinksSimulator
{
    class OrderDataProvider
    {
        private static readonly string connectionString = "Data Source=DESKTOP-VOU3DNV;Initial Catalog=QLQUAN;Integrated Security=True";
       
        #region FETCH LIST ORDER
        public void FetchListOrder()
        {

            // Provide the query string with a parameter placeholder.
            string queryString =
               "SELECT * FROM ORDER_DRINKS";

            // Specify the parameter value.


            // Create and open the connection in a using block. This
            // ensures that all resources will be closed and disposed
            // when the code exits.
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);


                // Open the connection in a try/catch block.
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    Console.WriteLine("OrderID\t\t\tDrinkID\t\t\tQuantity\t\t\tSale %");
                    while (reader.Read())
                    {
                        Console.WriteLine("{0}\t\t\t{1}\t\t\t{2}\t\t\t{3}",
                            reader[0], reader[1], reader[2], reader[3]);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.ReadLine();
            }
        }
        #endregion

        #region Create Bill
        public void AddOrder(string orderID, string drinkID, int quantity, int sale)
        {
            // Provide the query string with a parameter placeholder.
            string queryString =
               "INSERT INTO ORDER_DRINKS(MSDH, MSHH, SoLuong, TiLeGiam)" +
                " VALUES(@orderID,@drinkID,@quantity,@sale);";

            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.AddWithValue("@orderID", orderID);
                command.Parameters.AddWithValue("@drinkID", drinkID);
                command.Parameters.AddWithValue("@quantity", quantity);
                command.Parameters.AddWithValue("@sale", sale);

                // Open the connection in a try/catch block.
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                //Console.ReadLine();
                Console.WriteLine("Add Order Sucessfully");
            }
            #endregion
        }
    }
}
