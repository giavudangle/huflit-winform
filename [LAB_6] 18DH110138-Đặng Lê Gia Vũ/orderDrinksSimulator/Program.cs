﻿using System;
using System.Text;

namespace orderDrinksSimulator
{
    class Program
    {
        #region Drink Methods Section
        public static void handler_FetchListDrinks(DrinksDataProvider drinkDataController)
        {
            Console.WriteLine("\t\t\t___LIST DRINKS___\n");
            drinkDataController.FetchListDrinks();
        }

        public static void handler_AddDrink(DrinksDataProvider drinkDataController)
        {
            Console.Write("Enter Drink ID: ");
            string drinkID = Console.ReadLine();
            Console.Write("Enter Drink Name: ");
            string drinkName = Console.ReadLine();
            Console.Write("Enter Drink Is Available: ");
            int active = int.Parse(Console.ReadLine());
            Console.Write("Enter Price of Drink: ");
            int price = int.Parse(Console.ReadLine());

            drinkDataController.AddDrink(drinkID, drinkName, price, active);
            
        }
        public static void handler_DeleteDrink(DrinksDataProvider drinkDataController)
        {
            Console.Write("Enter Drink ID to DELETE: ");
            string drinkID = Console.ReadLine();
            drinkDataController.DeleteDrink(drinkID);
        }

        public static void handler_EditDrink(DrinksDataProvider drinkDataController)
        {
            Console.Write("Enter Current Drink ID: ");
            string drinkID = Console.ReadLine();         
            Console.Write("Enter Price You Want To Update: ");
            int price = int.Parse(Console.ReadLine());
            Console.Write("Enter Status Of Drink (1 or 0): ");
            int active = int.Parse(Console.ReadLine());
            Console.Write("Enter Drink Name You Want To Update: ");
            string drinkName = Console.ReadLine();
            drinkDataController.EditDrink(drinkID, drinkName, active,price );

        }
        #endregion
        #region Bill Methods Section
        public static void handler_FetchListBills(BillsDataProvider billDataController)
        {
            Console.WriteLine("\t___LIST BILLS___\n");
            billDataController.FetchListBills();
        }

        public static void handler_AddBill(BillsDataProvider billDataController)
        {
            Console.WriteLine("BILL CREATOR");
            DateTime dateTime = DateTime.Now;
            Console.Write("Enter Order ID To Create Bill: ");
            string billID = Console.ReadLine();
            billDataController.AddBill(billID, dateTime);
;        }
        #endregion

        #region Order Methods Section
        public static void handler_FetchListOrder(OrderDataProvider orderDataController)
        {
            Console.WriteLine("\t___LIST ORDER___\n");
            orderDataController.FetchListOrder();
        }

        public static void handler_AddOrder(OrderDataProvider orderDataController)
        {
            Console.WriteLine("____PLEASE CREATE ORDER____");
            Console.Write("Enter Order ID: ");
            string orderID = Console.ReadLine();
            Console.Write("Enter Drink ID: ");
            string drinkID = Console.ReadLine();
            Console.Write("Enter Quantity: ");
            int quantity =int.Parse(Console.ReadLine());
            Console.Write("Enter Sale %: ");
            int sale = int.Parse(Console.ReadLine());

            orderDataController.AddOrder(orderID, drinkID, quantity, sale);

        }

        #endregion

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            DrinksDataProvider drinksDataProvider = new DrinksDataProvider();
            BillsDataProvider billsDataProvider = new BillsDataProvider();
            OrderDataProvider orderDataProvider = new OrderDataProvider();

            /* CRUD DRINKS */
            //handler_FetchListDrinks(drinksDataProvider);   
            //handler_AddDrink(drinksDataProvider);
            //handler_DeleteDrink(drinksDataProvider);
            //handler_EditDrink(drinskDataProvider);

            /* Test fetch data */
            //handler_FetchListBills(billsDataProvider);        
            //handler_FetchListOrder(orderDataProvider);
            //handler_AddBill(billsDataProvider);

            /*Create Order -> Bill*/
            handler_FetchListDrinks(drinksDataProvider);
            handler_AddOrder(orderDataProvider);
            handler_AddBill(billsDataProvider);
            handler_FetchListBills(billsDataProvider);
            handler_FetchListOrder(orderDataProvider);


        }
    }
}
