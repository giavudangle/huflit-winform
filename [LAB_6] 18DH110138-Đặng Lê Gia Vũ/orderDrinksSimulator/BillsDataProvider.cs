﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace orderDrinksSimulator
{
    class BillsDataProvider
    {
        private static readonly string
        connectionString = "Data Source=DESKTOP-VOU3DNV;Initial Catalog=QLQUAN;Integrated Security=True";
        #region Fetch List Bills
        public void FetchListBills()
        {
            // Provide the query string with a parameter placeholder.
            string queryString = "SELECT * FROM BILL_DRINKS";
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    Console.WriteLine("OrderID\t\t\tOrder Information\n");
                    while (reader.Read())
                    {

                        Console.WriteLine("{0}\t\t\t{1}",
                            reader[0], reader[1]);
                    }

                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                //Console.ReadLine();

            }
        }
        #endregion
        #region Create Bill
        public void AddBill(string orderID, DateTime dateTime)
        {
            // Provide the query string with a parameter placeholder.
            string queryString =
               "INSERT INTO BILL_DRINKS(MSDH, NgayDat)" +
                " VALUES(@orderID,@dateTime)";

            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.AddWithValue("@orderID", orderID);
                command.Parameters.AddWithValue("@dateTime", dateTime.ToString());

                // Open the connection in a try/catch block.
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                //Console.ReadLine();
                Console.WriteLine("Add Bill Sucessfully");
            }
            #endregion

        }
    }
}
