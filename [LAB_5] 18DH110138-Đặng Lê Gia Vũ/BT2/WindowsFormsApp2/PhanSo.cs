﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    class PhanSo
    {
        private int tuSo;
        private int mauSo;

        public PhanSo()
        {

        }

        public PhanSo(int ts, int ms)
        {
            tuSo = ts;
            mauSo = ms;
        }
        private int GCD(int a, int b)
        {
            if (b != 0) return GCD(b, a % b);
            return a;

        }



        public void Nhap()
        {
            Console.Write("Nhap tu so ");
            tuSo = int.Parse(Console.ReadLine());
            Console.Write("Nhap mau so ");
            mauSo = int.Parse(Console.ReadLine());
        }

        public override string ToString()
        {
            // [3/5]
            return "[" + tuSo + "/" + mauSo + "]";
        }



        private PhanSo ToiGian()
        {

            int ucln = GCD(tuSo, mauSo);
            if (ucln == 1) return null ;
            PhanSo result = new PhanSo(tuSo / ucln, mauSo / ucln);
            return result;
         
        }



        public double GiaTriThapPhan()
        {
            // Add task
            return tuSo * 1.0 / mauSo;
        }

        public static PhanSo operator +(PhanSo p1, PhanSo p2)
        {
            PhanSo res = new PhanSo();
            res.tuSo = p1.tuSo * p2.mauSo + p2.tuSo * p1.mauSo;
            res.mauSo = p1.mauSo * p2.mauSo;
            PhanSo result = new PhanSo();
            result = res.ToiGian();
            return result;
        }

        public static PhanSo operator -(PhanSo p1, PhanSo p2)
        {
            PhanSo res = new PhanSo();
            res.tuSo = p1.tuSo * p2.mauSo - p2.tuSo * p1.mauSo;
            res.mauSo = p1.mauSo * p2.mauSo;
            PhanSo result = new PhanSo();
            result = res.ToiGian();
            return result;
        }




        public static PhanSo operator *(PhanSo p1, PhanSo p2)
        {
            PhanSo res = new PhanSo();
            res.tuSo = p1.tuSo * p2.tuSo;
            res.mauSo = p1.mauSo * p2.mauSo;
            PhanSo result = new PhanSo();
            result = res.ToiGian();
            return result;
        }
        public static PhanSo operator /(PhanSo p1, PhanSo p2)
        {
            // Add some conditions
            PhanSo res = new PhanSo();
            res.tuSo = p1.tuSo * p2.mauSo;
            res.mauSo = p1.mauSo * p2.tuSo;

            PhanSo result = new PhanSo();
            result = res.ToiGian();
            return result;
        }

        public static bool operator ==(PhanSo p1, PhanSo p2)
        {
            // So sanh tu mau
            if (p1.GiaTriThapPhan() == p2.GiaTriThapPhan()) return true;
            return false;
        }
        public static bool operator !=(PhanSo p1, PhanSo p2)
        {

            if ((p1 == p2)) return false;
            return true;
        }


        public static bool operator <(PhanSo p1, PhanSo p2)
        {

            if (p1.GiaTriThapPhan() < p2.GiaTriThapPhan()) return true;
            return false;
        }

        public static bool operator >(PhanSo p1, PhanSo p2)
        {

            if (p1.GiaTriThapPhan() > p2.GiaTriThapPhan()) return true;
            return false;
        }

        public static bool operator <=(PhanSo p1, PhanSo p2)
        {
            if (p1 < p2 || p1 == p2) return true;
            return false;
        }

        public static bool operator >=(PhanSo p1, PhanSo p2)
        {
            if (p1 > p2 || p1 == p2) return true;
            return false;
        }

    }
}
