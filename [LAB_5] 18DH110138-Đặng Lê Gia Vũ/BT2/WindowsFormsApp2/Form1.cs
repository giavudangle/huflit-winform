﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_result_Click(object sender, EventArgs e)
        {
            int ts_I = int.Parse(tuSo_I.Text);
            int ms_I = int.Parse(mauSo_I.Text);
            int ts_II = int.Parse(tuSo_II.Text);
            int ms_II = int.Parse(mauSo_II.Text);
            PhanSo ps1 = new PhanSo(ts_I, ms_I);
            PhanSo ps2 = new PhanSo(ts_II, ms_II);
            PhanSo res = ps1 + ps2;

            MessageBox.Show(res.ToString(), "Ket qua la: ");
        }
    }
}
