﻿namespace WindowsFormsApp2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tuSo_I = new System.Windows.Forms.TextBox();
            this.mauSo_I = new System.Windows.Forms.TextBox();
            this.tuSo_II = new System.Windows.Forms.TextBox();
            this.mauSo_II = new System.Windows.Forms.TextBox();
            this.btn_result = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nhap tu so phan so I :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nhap mau so phan so I : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 220);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nhap tu so phan so II :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 267);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nhap mau so phan so II :";
            // 
            // tuSo_I
            // 
            this.tuSo_I.Location = new System.Drawing.Point(184, 73);
            this.tuSo_I.Name = "tuSo_I";
            this.tuSo_I.Size = new System.Drawing.Size(119, 20);
            this.tuSo_I.TabIndex = 4;
            // 
            // mauSo_I
            // 
            this.mauSo_I.Location = new System.Drawing.Point(184, 119);
            this.mauSo_I.Name = "mauSo_I";
            this.mauSo_I.Size = new System.Drawing.Size(119, 20);
            this.mauSo_I.TabIndex = 5;
            // 
            // tuSo_II
            // 
            this.tuSo_II.Location = new System.Drawing.Point(184, 212);
            this.tuSo_II.Name = "tuSo_II";
            this.tuSo_II.Size = new System.Drawing.Size(119, 20);
            this.tuSo_II.TabIndex = 6;
            // 
            // mauSo_II
            // 
            this.mauSo_II.Location = new System.Drawing.Point(184, 267);
            this.mauSo_II.Name = "mauSo_II";
            this.mauSo_II.Size = new System.Drawing.Size(119, 20);
            this.mauSo_II.TabIndex = 7;
            // 
            // btn_result
            // 
            this.btn_result.Location = new System.Drawing.Point(55, 334);
            this.btn_result.Name = "btn_result";
            this.btn_result.Size = new System.Drawing.Size(75, 23);
            this.btn_result.TabIndex = 8;
            this.btn_result.Text = "Calculate";
            this.btn_result.UseVisualStyleBackColor = true;
            this.btn_result.Click += new System.EventHandler(this.btn_result_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_result);
            this.Controls.Add(this.mauSo_II);
            this.Controls.Add(this.tuSo_II);
            this.Controls.Add(this.mauSo_I);
            this.Controls.Add(this.tuSo_I);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tuSo_I;
        private System.Windows.Forms.TextBox mauSo_I;
        private System.Windows.Forms.TextBox tuSo_II;
        private System.Windows.Forms.TextBox mauSo_II;
        private System.Windows.Forms.Button btn_result;
    }
}

