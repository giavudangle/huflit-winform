﻿namespace WindowsFormsApp3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.maNV = new System.Windows.Forms.TextBox();
            this.tenNV = new System.Windows.Forms.TextBox();
            this.nv_kinhdoanh = new System.Windows.Forms.RadioButton();
            this.nv_sanxuat = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.result = new System.Windows.Forms.Label();
            this.btn_calculate = new System.Windows.Forms.Button();
            this.luong_co_ban = new System.Windows.Forms.TextBox();
            this.so_hop_dong = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.so_luong_sx = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ma NV :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ten NV :";
            // 
            // maNV
            // 
            this.maNV.Location = new System.Drawing.Point(124, 58);
            this.maNV.Name = "maNV";
            this.maNV.Size = new System.Drawing.Size(226, 20);
            this.maNV.TabIndex = 2;
            // 
            // tenNV
            // 
            this.tenNV.Location = new System.Drawing.Point(124, 111);
            this.tenNV.Name = "tenNV";
            this.tenNV.Size = new System.Drawing.Size(226, 20);
            this.tenNV.TabIndex = 3;
            // 
            // nv_kinhdoanh
            // 
            this.nv_kinhdoanh.AutoSize = true;
            this.nv_kinhdoanh.Location = new System.Drawing.Point(132, 164);
            this.nv_kinhdoanh.Name = "nv_kinhdoanh";
            this.nv_kinhdoanh.Size = new System.Drawing.Size(102, 17);
            this.nv_kinhdoanh.TabIndex = 4;
            this.nv_kinhdoanh.TabStop = true;
            this.nv_kinhdoanh.Text = "NV Kinh Doanh ";
            this.nv_kinhdoanh.UseVisualStyleBackColor = true;
            this.nv_kinhdoanh.CheckedChanged += new System.EventHandler(this.nv_kinhdoanh_CheckedChanged);
            // 
            // nv_sanxuat
            // 
            this.nv_sanxuat.AutoSize = true;
            this.nv_sanxuat.Location = new System.Drawing.Point(472, 164);
            this.nv_sanxuat.Name = "nv_sanxuat";
            this.nv_sanxuat.Size = new System.Drawing.Size(87, 17);
            this.nv_sanxuat.TabIndex = 5;
            this.nv_sanxuat.TabStop = true;
            this.nv_sanxuat.Text = "NV San Xuat";
            this.nv_sanxuat.UseVisualStyleBackColor = true;
            this.nv_sanxuat.CheckedChanged += new System.EventHandler(this.nv_sanxuat_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(124, 206);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Luong co ban ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(129, 249);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "So hop dong";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(265, 388);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Luong cua nhan vien la : ";
            // 
            // result
            // 
            this.result.AutoSize = true;
            this.result.Location = new System.Drawing.Point(399, 388);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(13, 13);
            this.result.TabIndex = 9;
            this.result.Text = "0";
            // 
            // btn_calculate
            // 
            this.btn_calculate.Location = new System.Drawing.Point(317, 331);
            this.btn_calculate.Name = "btn_calculate";
            this.btn_calculate.Size = new System.Drawing.Size(144, 23);
            this.btn_calculate.TabIndex = 10;
            this.btn_calculate.Text = "Tinh luong nhan vien";
            this.btn_calculate.UseVisualStyleBackColor = true;
            this.btn_calculate.Click += new System.EventHandler(this.btn_calculate_Click);
            // 
            // luong_co_ban
            // 
            this.luong_co_ban.Location = new System.Drawing.Point(206, 203);
            this.luong_co_ban.Name = "luong_co_ban";
            this.luong_co_ban.Size = new System.Drawing.Size(122, 20);
            this.luong_co_ban.TabIndex = 11;
            // 
            // so_hop_dong
            // 
            this.so_hop_dong.Location = new System.Drawing.Point(206, 246);
            this.so_hop_dong.Name = "so_hop_dong";
            this.so_hop_dong.Size = new System.Drawing.Size(122, 20);
            this.so_hop_dong.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(469, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "So luong SX";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // so_luong_sx
            // 
            this.so_luong_sx.Location = new System.Drawing.Point(541, 203);
            this.so_luong_sx.Name = "so_luong_sx";
            this.so_luong_sx.Size = new System.Drawing.Size(144, 20);
            this.so_luong_sx.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.so_luong_sx);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.so_hop_dong);
            this.Controls.Add(this.luong_co_ban);
            this.Controls.Add(this.btn_calculate);
            this.Controls.Add(this.result);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nv_sanxuat);
            this.Controls.Add(this.nv_kinhdoanh);
            this.Controls.Add(this.tenNV);
            this.Controls.Add(this.maNV);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox maNV;
        private System.Windows.Forms.TextBox tenNV;
        private System.Windows.Forms.RadioButton nv_kinhdoanh;
        private System.Windows.Forms.RadioButton nv_sanxuat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label result;
        private System.Windows.Forms.Button btn_calculate;
        private System.Windows.Forms.TextBox luong_co_ban;
        private System.Windows.Forms.TextBox so_hop_dong;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox so_luong_sx;
    }
}

