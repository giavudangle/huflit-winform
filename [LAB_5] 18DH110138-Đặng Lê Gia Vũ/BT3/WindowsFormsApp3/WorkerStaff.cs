﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    public class WorkerStaff : Staff
    {
        private int product;

        const int price = 1000;
        const int bound = 3000;



        public int Product
        {
            get => product;
            set => product = value;
        }


        public WorkerStaff(string id, string name, int product) : base(id, name)
        {
            this.product = product;
        }
        // Them parameter constructor + bo input ra khoi class
        public WorkerStaff() : base()
        {
            base.staffInput();
            Console.Write("Nhap so luong san pham cua nhan vien san xuat: ");
            product = int.Parse(Console.ReadLine());
        }

        public override int Salary()
        {
            return product >= 3000
                ? (price * product + (int)(price * product * 0.05))
                : price * product;


        }

        public override string ToString()
        {
            return
               "___THONG TIN NHAN VIEN SAN XUAT___\n"
                + base.ToString()
                + "Luong thuc te la: " + Salary() + "\n";
        }

    }
}
