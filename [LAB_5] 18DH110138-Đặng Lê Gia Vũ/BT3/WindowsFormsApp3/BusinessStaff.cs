﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    public class BusinessStaff : Staff
    {
        private int baseSalary;
        private int contact;

        const int BONUS = 500000;


        public int BaseSalary
        {
            get => baseSalary;
            set => baseSalary = value;
        }

        // Add more const
        public int Contact
        {
            get => contact;
            set => contact = value;
        }

        


        public BusinessStaff(string id, string name, int baseSalary, int contact) : base(id, name)
        {
            this.baseSalary = baseSalary;
            this.contact = contact;
        }
      
        public BusinessStaff() : base()
        {
            base.staffInput();
            Console.Write("Nhap luong co ban cua nhan vien kinh doanh: ");
            baseSalary = int.Parse(Console.ReadLine());
            Console.Write("Nhap so hop dong da ki ket: ");
            contact = int.Parse(Console.ReadLine());
        }

        public override int Salary()
        {
            return baseSalary += contact * BONUS;
        }

        


        public override string ToString()
        {
            return
                "___THONG TIN NHAN VIEN KINH DOANH___\n"
                + base.ToString()
                + "Luong thuc te la: " + Salary() + "\n";

        }


    }
}
