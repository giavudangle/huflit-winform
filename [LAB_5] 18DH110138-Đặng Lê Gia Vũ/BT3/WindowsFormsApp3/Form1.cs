﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Init_Template();
        }

        private void Init_Template()
        {
            this.nv_kinhdoanh.Checked = true;
            this.nv_sanxuat.Checked = false;
        }

        private void nv_kinhdoanh_CheckedChanged(object sender, EventArgs e)
        {
            this.clear_Data_View();
            this.so_luong_sx.Enabled = false;
            this.luong_co_ban.Enabled = true;
            this.so_hop_dong.Enabled = true;
        }

        private void nv_sanxuat_CheckedChanged(object sender, EventArgs e)
        {
            this.clear_Data_View();
            this.so_hop_dong.Enabled = false;
            this.luong_co_ban.Enabled = false;
            this.so_luong_sx.Enabled = true;
            
        }

        private void clear_Data_View()
        {
            this.so_hop_dong.Text = "";
            this.so_luong_sx.Text = "";
            this.luong_co_ban.Text = "";

        }

        private void btn_calculate_Click(object sender, EventArgs e)
        {
            Staff staff;
            string staffID = maNV.Text;
            string fullName = tenNV.Text;

            bool typeStaff = this.nv_kinhdoanh.Checked;
            if (typeStaff)
            {
                int salary = int.Parse(luong_co_ban.Text);
                int contact = int.Parse(so_hop_dong.Text);
                staff = new BusinessStaff(staffID, fullName, salary, contact);
            }
            else
            {
                int product = int.Parse(so_luong_sx.Text);
                staff = new WorkerStaff(staffID, fullName, product);
            }

            this.result.Text = staff.Salary().ToString();
        }

    }
}
