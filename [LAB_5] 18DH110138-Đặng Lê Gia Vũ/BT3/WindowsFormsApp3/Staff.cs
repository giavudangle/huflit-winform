﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    public abstract class Staff
    {
        private string staffId;
        private string fullName;

        // docs constructor
        public Staff()
        {

        }
        public Staff(string id, string name)
        {
            staffId = id;
            fullName = name;
        }

        public string StaffID
        {
            get => staffId;
            set => staffId = value;
        }

        public string FullName
        {
            get => fullName;
            set => fullName = value;
        }

        // Create abstraction Salary *note
        public abstract int Salary();

        // Bo khoi lop
        public void staffInput()
        {
            Console.Write("Nhap ma nhan vien: ");
            staffId = Console.ReadLine();
            Console.Write("Nhap ho ten nhan vien: ");
            fullName = Console.ReadLine();
        }

        public override string ToString()
        {
            return
                "Ma nhan vien: " + StaffID + "\n"
                + "Ho ten: " + FullName + "\n";
        }
    }
}
